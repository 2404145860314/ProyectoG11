var mysql = require('mysql');
var express = require('express')
const morgan = require('morgan')
const dayjs = require('dayjs');
const fs = require('fs')
var bodyParser = require('body-parser')
var app = express()
const cors = require('cors')

var corsOptions = { origin: true, optionsSuccessStatus: 200 };
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
var port = 2000;


app.use(morgan('dev'))

var connection = mysql.createConnection({
   host: '34.121.159.67',//'localhost' | 34.121.159.67
   user: 'root',
   password: 'root',//local - 12345678|Conexion - root
   database: 'saproyecto',//'semi1'
   port: 3306
});

connection.connect(function(error){
   if(error){
      throw error;
   }else{
      console.log('Conexion correcta.');
   }
});

app.post('/', function(req, res){
   res.send("<h1>BIENVENIDO</h1>")
})

app.post('/RegistrarPersona', function(req, res){
   try {
      const {cui, nombre, apellido, telefono, foto, pin, correo, mac, tipo, pais, estado, id_municipio} = req.body
      let sql = `insert into Persona values(${cui}, '${nombre}', '${apellido}', '${telefono}', '${foto}', ${pin}, '${correo}', '${mac}', '${tipo}', '${pais}', '${estado}', ${id_municipio});`
      connection.query(sql, function(err, result){
         if(err) throw res.status(404).json({
            estado: false,
            status: 404,
            error: 'error al registrar usuario'
         });
         return res.status(200).json({
            estado: true,
            status: 200,
            mensaje: "Usuario registrado"
         });
      });
   } catch (error) {
      res.status(404).json({
         estado: false,
         status: 404,
         error: "error en la peticion: RegistrarPersona"
      })
   }
})

app.post('/AsignarPersonaEleccion', function(req, res){
   try {
      let today = dayjs();
      let day = today.format("YYYY-MM-DD h:mm:ss").toString()
      const {id_persona, id_eleccion} = req.body

      let sql1 = `select * from Asignar_eleccion where id_persona = ${id_persona} and id_eleccion = ${id_eleccion};`
      connection.query(sql1, function(err, result){
         if(err) throw res.status(404).json({
            estado: false,
            status: 404,
            error: "error al verificar"
         });
         if(JSON.stringify(result) == "[]"){
            let sql = `insert into Asignar_eleccion (fecha, id_persona, id_eleccion, estado) values ('${day}', '${id_persona}', '${id_eleccion}', 0);`
            connection.query(sql, function(err, result){
               if(err) throw res.status(404).json({
                  estado: false,
                  status: 404,
                  error: "error al asignar usuario a votacion"
               });
               return res.status(200).json({
                  estado: true,
                  status: 200,
                  mensaje: "Usuario asignado a la eleccion"
               });
            });
         } else {
            return res.status(200).json({
               estado: true,
               status: 200,
               mensaje: "Usuario ya asignado"
            });
         }
      })
   } catch (error) {
      res.status(404).json({
         estado: false,
         status: 404,
         error: "error en la peticion: AsignarPersonaEleccion"
      })
   }
})

app.post('/Votar', function(req, res){
   try {
      let today = dayjs();
      let day = today.format("YYYY-MM-DD h:mm:ss").toString()
      const {estado, aprobado, ubicacion, id_candidato, id_persona, id_eleccion} = req.body
      let sql = `insert into Votacion (fecha, estado, aprobado, ubicacion, id_candidato, id_persona) values ('${day}', '${estado}', ${aprobado}, '${ubicacion}', ${id_candidato}, ${id_persona});`
      connection.query(sql, function(err, result){
         if(err) throw res.status(404).json({
            estado: false,
            status: 404,
            error: "error al realizar la votacion"});

         let sqlUp = `update Asignar_eleccion set estado = 1 where id_persona = ${id_persona} and id_eleccion = ${id_eleccion};`
         connection.query(sqlUp)
         return res.status(200).json({
            estado: true,
            status: 200,
            mensaje: "Se registro el voto del usuario"});
      });
   } catch (error) {
      res.status(404).json({
         estado: false,
         status: 404,
         "error": 'error en la peticion: Votar'
      })
   }
})

app.get('/VotosPresenciales', function(req, res){
   try {
      let rawdata = fs.readFileSync('votos.json');
      let votos = JSON.parse(rawdata);
      let registrados = 0, errados = 0;
      votos['votos'].forEach(element => {
         let sql = `insert into Votacion (estado, aprobado, ubicacion, id_candidato, id_persona) 
            values ('${element.estado}', ${element.aprobado}, '${element.ubicacion}', ${element.id_candidato}, ${element.id_persona});`
         connection.query(sql, function(err, result){
            if(err) {
               errados += 1
               return
            } 
            registrados += 1
            if(votos['votos'][votos['votos'].length-1] === element){
               return res.status(200).json({
                  estado: true,
                  status: 200,
                  mensaje: "Se registraron los votos",
                  registros_exitosos: registrados,
                  registros_errados: errados
               });
            }
         });
      });
   } catch (error) {
      res.status(404).json({
         estado: false,
         status: 404,
         "error": 'error en la peticion: VotosPresenciales'
      })
   }
})

app.get('/datos', function(req, res){
   try {
      let sql = `select * from Persona`
      connection.query(sql, function(err, result){
         if(err) throw res.status(404).json({"error": 'error al registrar usuario'});
         return res.status(200).json(result);
      });
   } catch (error) {
      res.status(404).json({"error": 'error en la peticion: datos'})
   }
})

app.post('/elecciones', function(req, res){
   try {
      let sql = `select id_eleccion, titulo, descripcion from Eleccion;`
      connection.query(sql, function(err, result){
         if(err) throw res.status(404).json({"error": 'error al obtener las elecciones'});
         return res.status(200).json(result);
      });
   } catch (error) {
      res.status(404).json({"error": 'error en la peticion: datos'})
   }
})

app.post('/GetVotaciones', function(req, res){
   try {
      const {id_persona} = req.body
      let sql = `select a.id_eleccion, a.titulo, a.descripcion from Eleccion as a, Asignar_eleccion as b, Persona as c
         where a.id_eleccion = b.id_eleccion and b.id_persona = c.cui and b.id_persona = ${id_persona} and b.estado = 0;`
      connection.query(sql, function(err, result){
         if(err) throw res.status(404).json({"error": 'error en GetVotaciones en los parametros'});
         return res.status(200).json(result);
      });
   } catch (error) {
      res.status(404).json({"error": 'error en la peticion: GetVotaciones'})
   }
})

app.post('/GetPostulados', function(req, res){
   try {
      const {id_eleccion} = req.body
      let sql = `select a.cui, a.partido, a.foto, a.imagen_partido from Candidato as a, Eleccion as b
      where a.id_eleccion = b.id_eleccion and a.id_eleccion = ${id_eleccion}`
      connection.query(sql, function(err, result){
         if(err) throw res.status(404).json({"error": 'error en GetPostulados en los parametros'});
         return res.status(200).json(result);
      });
   } catch (error) {
      res.status(404).json({"error": 'error en la peticion: GetPostulados'})
   }
})

module.exports = app.listen(port, () => {
   console.log(`Listening on port ${port}`);
});
