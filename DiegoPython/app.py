from flask import Flask, jsonify, request
import json
from flask_cors import CORS
import pymysql

app = Flask(__name__)

db = pymysql.connect( host="34.121.159.67", port=3306, user="root", password="root", db="saproyecto")
if(db):
    print("Conexion exitosa")

cursor = db.cursor()

CORS(app)

@app.route('/Registro', methods = ['POST'])
def index():
    try:
        solicitado = request.get_json()
        cursor.execute("Insert into Registro (cui, nombre, expediente) values ("+str(solicitado['cui'])+", '"+solicitado['nombre']+"', "+str(solicitado['expediente'])+")")
        db.commit()
        #cursor.close()
        return jsonify({
            "estado": True,
            "status": 200,
            "mensaje": "Expediente registrado"
            })
    except Exception as e:
        print(e)
        return jsonify({
            "estado": False,
            "status": 400,
            "mensaje": "Fallo en el registro de expedientes"
            })
    

@app.route('/ConsultarExpediente', methods = ['POST'])
def peticion2():
    try:
        solicitado = request.get_json()
        print(solicitado['cui'])
        query = "select expediente from Registro where cui="+str(solicitado['cui'])
        cursor.execute(query)
        row_headers = [x[0] for x in cursor.description]  #
        data = cursor.fetchall()
        print(data)
        json_data = []
        for result in data:
            json_data.append(dict(zip(row_headers, result)))

        return jsonify({"estado": True, "status": 200, "data": json_data}), 200
    except Exception as e:
        print(e)
        return jsonify({"estado": False, "status": 400, "mensaje": "Los expedientes"}), 400
    

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=2500, debug=True)