import { importProvidersFrom, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


//components
import { TasksComponent } from './components/tasks/tasks.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { ListaEleccionesComponent } from './components/lista-elecciones/lista-elecciones.component';
import { GanadorComponent } from './components/ganador/ganador.component';
import { EleccionComponent } from './components/eleccion/eleccion.component';
import { MantenimientoEleccionComponent } from './components/mantenimiento-eleccion/mantenimiento-eleccion.component';
import { MantenimientoCandidatoComponent } from './components/mantenimiento-candidato/mantenimiento-candidato.component';
import { VotosComponent } from './components/votos/votos.component';
import { RenapMasivoComponent } from './components/renap-masivo/renap-masivo.component';
import { AuditoriaComponent } from './components/auditoria/auditoria.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/signin',
    pathMatch: 'full'
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'relecciones',
    component: ListaEleccionesComponent
  },
  {
    path: 'ganador/:id',
    component: GanadorComponent
  },
  {
    path: 'auditoria',
    component: AuditoriaComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'tasks',
    component: TasksComponent
  },
  {
    path: 'Eleccion',
    component: EleccionComponent
  },
  {
    path: 'Mantenimiento-Eleccion',
    component: MantenimientoEleccionComponent
  },
  {
    path: 'Mantenimiento-Candidato/:id',
    component: MantenimientoCandidatoComponent
  },
  {
    path: 'Renap-Masivo',
    component: RenapMasivoComponent
  },
  {
    path: 'Votos',
    component: VotosComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
