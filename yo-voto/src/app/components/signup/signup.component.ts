import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { HttpErrorResponse } from '@angular/common/http';
import getMAC, { isMAC } from 'getmac'
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  user = {
    cui: null,
    nombre: null,
    apellido: null,
    telefono: null,
    foto: null,
    pin: null,
    correo: null,
    mac: null,
    tipo: null,
    pais: null,
    estado: null,
    id_municipio: null
  }


  usRenap:any = [];
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  signUp() {
    let cuiUser = {
      cui: this.user.cui
    }

    this.authService.validarUserRenap(cuiUser)
    .subscribe(
      res => {
        console.log(res)
        console.log('Otros')
        //console.log(res.data[0].expediente)
        //if (res instanceof HttpErrorResponse) {
          if (res.data[0]?.expediente === 1) {
            console.log('Entre a status')
            window.alert("Votante Apto para votar");

            this.authService.signUp(this.user)
            .subscribe(
              res => {
                //this.router.navigate(['/signin']);
                console.log(res);

              },
              err => console.log(err)
            )
          } else{
            window.alert("Usuario no apto para votar");
          }
      }

    )
  }

}
