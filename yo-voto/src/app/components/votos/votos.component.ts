import { Component, OnInit } from '@angular/core';
import { VotoService } from 'src/app/services/votos.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-votos',
  templateUrl: './votos.component.html',
  styleUrls: ['./votos.component.css']
})
export class VotosComponent implements OnInit {

  constructor(private VotoService: VotoService, private router:Router, public location: Location) { }
  public cui = ""
  public votacion = ""

  ngOnInit(): void {
  }

  votos: any
  voto: any
  async votar(){
    let res = await this.VotoService.getVotaciones(this.cui)
    let json=JSON.stringify(res)
    let obj= JSON.parse(json)
    this.votos = obj
  }

  postulados: any
  postulado: any
  async GetPostulados(){
    let res = await this.VotoService.getPostulados(this.votacion)
    let json=JSON.stringify(res)
    let obj= JSON.parse(json)
    this.postulados = obj
  }

  async Elegir(dato: string){
    let res = await this.VotoService.SendVoto(dato, this.cui, this.votacion)
    let json=JSON.stringify(res)
    let obj= JSON.parse(json)
    
    window.location.reload();
    alert(obj.mensaje)
  }

  refresh(): void {
		this.router.navigateByUrl("/Votos", { skipLocationChange: true }).then(() => {
		console.log(decodeURI(this.location.path()));
		this.router.navigate([decodeURI(this.location.path())]);
		});
	}

}
