import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MantenimientoEleccionService } from '../../services/eleccion/mantenimiento-eleccion.service';

@Component({
  selector: 'app-mantenimiento-eleccion',
  templateUrl: './mantenimiento-eleccion.component.html',
  styleUrls: ['./mantenimiento-eleccion.component.css']
})
export class MantenimientoEleccionComponent implements OnInit {
  elecciones: any = []
  dataForm = this.fb.group({
    titulo: [''],
    descripcion: [''],
    inicio: [''],
    h_inicio: [''],
    fin: [''],
    h_fin: [''],
    ubicacion: ['']
  });
  constructor(
    private fb: FormBuilder,
    private mantenimientoEleccionService: MantenimientoEleccionService
  ) {
    this.getUsuarios()
  }

  ngOnInit(): void {
  }

  getUsuarios() {
    this.mantenimientoEleccionService.getElecciones().then((res: any) => {
      this.elecciones = res;
      console.log(this.elecciones)
    });
  }


  crear() {
    const data = this.dataForm.value;
    const eleccion = {
      titulo: data.titulo,
      descripcion: data.descripcion,
      fecha_ini: data.inicio,
      fecha_fin: data.fin,
      ubicacion: data.ubicacion
    }
    console.log(eleccion);
    this.mantenimientoEleccionService.crearEleccion(eleccion).then((res: any) => {
      if (res.estado) {
        this.dataForm = this.fb.group({
          titulo: [''],
          descripcion: [''],
          inicio: [''],
          h_inicio: [''],
          fin: [''],
          h_fin: [''],
          ubicacion: ['']
        });
        this.getUsuarios();
      } else {
        console.log(res) 
      }
    });

  }
}
