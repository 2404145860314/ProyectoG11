import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoEleccionComponent } from './mantenimiento-eleccion.component';

describe('MantenimientoEleccionComponent', () => {
  let component: MantenimientoEleccionComponent;
  let fixture: ComponentFixture<MantenimientoEleccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MantenimientoEleccionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MantenimientoEleccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
