import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenapMasivoComponent } from './renap-masivo.component';

describe('RenapMasivoComponent', () => {
  let component: RenapMasivoComponent;
  let fixture: ComponentFixture<RenapMasivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenapMasivoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RenapMasivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
