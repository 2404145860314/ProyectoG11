import { Component, OnInit } from '@angular/core';
import { MantenimientoEleccionService } from 'src/app/services/eleccion/mantenimiento-eleccion.service';

@Component({
  selector: 'app-renap-masivo',
  templateUrl: './renap-masivo.component.html',
  styleUrls: ['./renap-masivo.component.css']
})
export class RenapMasivoComponent implements OnInit {
  profile: string | undefined = "";
  constructor(
    private mantenimientoEleccionService: MantenimientoEleccionService
  ) { }

  ngOnInit(): void {
  }

  handleUpload(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    
    reader.readAsText(file);
    reader.onload = () => {
      //TODO save the base64 data in local
      var filestring = reader.result?.toString();
      //console.log('cargando archivo')
      this.profile = filestring;
      console.log(this.profile);
    };
  }

  crear() {
    const data = this.profile?.split('\n');
    data?.forEach(element => {
      const eleccion = {
        cui: element.split(',')[0],
        nombre: element.split(',')[1],
        expediente: element.split(',')[2] == 'true'?0:1
      }
      console.log(eleccion);
      this.mantenimientoEleccionService.createRENAP(eleccion).then((res: any) => {
        if (res.estado) {
          alert('Cargado')
        } else {
          alert(res) 
        }
      });
  
    });
    
  }

}
