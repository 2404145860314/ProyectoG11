import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CandidatoService } from '../../services/candidato/candidato.service';

@Component({
  selector: 'app-mantenimiento-candidato',
  templateUrl: './mantenimiento-candidato.component.html',
  styleUrls: ['./mantenimiento-candidato.component.css']
})
export class MantenimientoCandidatoComponent implements OnInit {
  id: any;
  profile: string | undefined = "";
  logo: string | undefined = "";
  JSON:any = JSON
  candidatos: any = []
  departamentos:any = []
  municipios:any = []
  departamentoSelect:any = ''
  dataForm = this.fb.group({
    cui: [''],
    nombre: [''],
    partido: [''],
    foto: [''],
    logo_partido: [''],
    departamento: [''],
    municipio: ['']
  });
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private candidatoService: CandidatoService
  ) {
    this.id = this.route.snapshot.paramMap.get('id')
    console.log(this.id)
    this.getCandidatos();
    this.getDepartamentos();
  }

  ngOnInit(): void {

  }

  selectDepartamento(event:any){
    let d = JSON.parse((event.target as HTMLInputElement).value)
    this.departamentoSelect = d.nombre
    console.log(JSON.parse((event.target as HTMLInputElement).value))
    this.getMunicipios(d.id_departamento)
  }

  getCandidatos() {
    this.candidatoService.getCandidatos(this.id).then((res: any) => {
      this.candidatos = res.data;
      console.log(this.candidatos)
    });
  }

  getDepartamentos() {
    this.candidatoService.getDepartamentos().then((res: any) => {
      this.departamentos = res;
      console.log(this.departamentos)
    });
  }

  getMunicipios(id_departamento:any) {
    this.candidatoService.getMunicipios(id_departamento).then((res: any) => {
      this.municipios = res;
      console.log(this.municipios)
    });
  }


  handleUpload(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      //TODO save the base64 data in local
      var filestring = reader.result?.toString();
      this.profile = filestring?.split(',')[1];
      //console.log(this.profile);
    };
  }

  handleUploadLogo(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      //TODO save the base64 data in local
      var filestring = reader.result?.toString();
      this.logo = filestring?.split(',')[1];
      //console.log(this.profile);
    };
  }

  crear() {
    const data = this.dataForm.value;
    const user = {
      cui: data.cui,
      nombre: data.nombre,
      partido: data.partido,
      foto: this.profile,
      imagen_partido: this.logo,
      departamento: this.departamentoSelect,
      municipio: data.municipio,
      id_eleccion: this.id
    }
    if (this.profile != "" && this.logo != "") {
      console.log(user);
      this.candidatoService.crearCandidato(user).then((res: any) => {
        if (res.estado) {
          this.getCandidatos()
          this.dataForm = this.fb.group({
            cui: [''],
            nombre: [''],
            partido: [''],
            foto: [''],
            logo_partido: [''],
            departamento: [''],
            municipio: ['']
          });
        } else {
          alert(res)
        }
      });
    } else {
      alert('seleccione imagenes')
    }

  }
}
