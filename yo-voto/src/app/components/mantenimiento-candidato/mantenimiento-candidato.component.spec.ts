import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoCandidatoComponent } from './mantenimiento-candidato.component';

describe('MantenimientoCandidatoComponent', () => {
  let component: MantenimientoCandidatoComponent;
  let fixture: ComponentFixture<MantenimientoCandidatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MantenimientoCandidatoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MantenimientoCandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
