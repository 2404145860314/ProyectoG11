import { Component, ElementRef, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResultadosService } from 'src/app/services/resultados.service';

import { ChartType, ChartOptions,ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

import {  timer,interval} from "rxjs";
import { AgmMap, MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-ganador',
  templateUrl: './ganador.component.html',
  styleUrls: ['./ganador.component.css']
})
export class GanadorComponent implements OnInit {

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };

  public barChartOptions: ChartOptions = {
    responsive: true,
  };

  public barChartLabels: Label[] = [' ','2022'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [

  ];

  icon = {
    url: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
    scaledSize: {
      width: 30,
      height: 30
    }
}

  ////MAPA
  title: string = 'AGM project';
  latitude: number=15.783471;
  longitude: number=-90.230759;
  zoom=8;
  @ViewChild('search')
  public searchElementRef!: ElementRef;
  markers = [
    {
      lat: 16.9,
      lng: -89.9,
      label: 'Peten'

    },{
        lat:14.090744653438819,
        lng:-90.98535472922623,
        label:'Escuintla'
    },{
      lat:14.42963293168057,
      lng:-91.41683310055183,
      label:'Suchitepequez'
    },{
      lat:14.413860963325718,
      lng:-91.81971591107674,
      label:'Retalhuleu'
    }
    ,{
      lat:15.00903692649161,
      lng:-91.9244143518301,
      label:'San Marcos'
    }
    ,{
      lat:15.690356545054671,
      lng:-91.5530860326853,
      label:'Huehuetenango'
    },{
      lat:15.317488753088398,
      lng:-90.9881737547005,
      label:'Quiche'
    },{
      lat:15.003963851078499,
      lng:-91.41198499143265,
      label:'Totonicapan'
    }
    ,{
      lat:14.823745885782577,
      lng:-91.61641466446362,
      label:'Quetzaltenango'
    },{
      lat:14.720397106009992,
      lng:-91.30325445638266,
      label:'Solola'
    },{
      lat:14.722721471572545,
      lng:-90.89794291847613,
      label:'Chimaltenango'
    },{
      lat:14.527859926040437,
      lng:-90.76003844512886,
      label:'Sacatepequez'
    },{
      lat:14.59933421005838,
      lng:-90.48313758302933,
      label:'Guatemala'
    },{
      lat:14.159087271973862,
      lng:-90.34509989509034,
      label:'Santa Rosa'
    },{
      lat:14.217594881468438,
      lng:-89.88054615521479,
      label:'Jutiapa'
    },{
      lat:14.630565525787494,
      lng:-89.93427857136818,
      label:'Jalapa'
    },{
      lat:14.683045320653799,
      lng:-89.44225901118013,
      label:'Chiquimula'
    },{
      lat:14.98592360625444,
      lng:-89.53166037694213,
      label:'Zacapa'
    },{
      lat:14.885595656170862,
      lng:-90.09982148081657,
      label:'El Progreso'
    },{
      lat:15.511971089510373,
      lng:-89.10398826778794,
      label:'Izabal'
    },{
      lat: 15.62651763809834,
      lng: -90.16720640432503,
      label:'Alta Verapaz'
    },{
      lat:15.110631153666557,
      lng:-90.4709530947298,
      label:'Baja Verapaz'
    }


  ]




  markers2 = [
    {
      lat:14.558881715696343,
      lng:-90.73283040362861,
      label: 'Antigua Guatemala'

    },{
        lat:14.485677303646494,
        lng:-90.80481871198347,
        label:'Alotenango'
    },{
      lat:14.521951956052034,
      lng:-90.76137628455166,
      label:'Ciudad Vieja'
    },{
      lat:14.548220948899642,
      lng:-90.78034409553992,
      label:'San Antonio'
    }
    ,{
      lat:14.552454979619004,
      lng:-90.78792685238368,
      label:'Santa Catarina'
    }
    ,{
      lat:14.521320249311739,
      lng: -90.79900679618893,
      label:'Dueñas'
    }
  ]
 /////////////////////////

  listaTotal:any=[];
  listaCandidatos:any=[];
  lgraficas:any;
  listabarras:any=[];
  listaubicacion:any=[];
  public pieChartLabels: Label[] =this.listaCandidatos;
  public pieChartData: SingleDataSet = this.listaTotal;
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor(private servResult:ResultadosService, private active:ActivatedRoute,private mapsAPILoader: MapsAPILoader,private ngZone: NgZone,private rout:Router) { 

    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
    this.pieChartLabels=this.listaCandidatos;
    this.pieChartData = this.listaTotal;
    this.pieChartType = 'pie';
    this.pieChartLegend = true;
    this.pieChartPlugins = [];

  }


  ganador:any;
  cui:any="";
  porcentaje:any=0;
  
  resultregis:any;
  resultvot:any;

  registrados:number=0;
  votantes:number=0;
  errorUbicacion:string=""
  
  ngOnInit(): void {

    

    const cronometro=interval(3000);
    cronometro.subscribe((n)=>{
      this.listaTotal=[];
      this.listaCandidatos=[];
      this.lgraficas=[];
      this.listabarras=[];
      this.pieChartLabels=[];
      this.pieChartData = [];
      this.barChartData=[];
      this.ObtenerGanador();
      this.ObtenerRegistradas();
      this.ObtenerVotantes();
      this.ObtenerCandidatos()
    });

    this.mapsAPILoader.load().then(() => {
    });
  }


  onMapClicked(event: any){
    console.table(event.coords);
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
  }
  
  ObtenerGanador(){
      let id=this.active.snapshot.params["id"];
      this.servResult.ObtenerGanador(id).subscribe(
        result=>{
            this.ganador=result;
            this.cui=this.ganador.rows[0].cui;

            this.porcentaje=Number(this.ganador.porcentaje*100).toFixed(2);
        },error=>{
          console.log(error)
        }
      );
  }

  ObtenerRegistradas(){
      this.servResult.ObtenerRegistrados().subscribe(
        result=>{
          this.resultregis=result;
          this.registrados=this.resultregis.rows[0].total;
      },error=>{
        console.log(error)
      } 
      );
  }

  ObtenerVotantes(){
    this.servResult.ObtenerVotaciones().subscribe(
      result=>{
        this.resultvot=result;
        this.votantes=this.resultvot.rows[0].total;
    },error=>{
      console.log(error)
    } 
    );
}

ObtenerCandidatos(){
  let id=this.active.snapshot.params["id"];
  this.servResult.ObtenerCandidatos(id).subscribe(
    result=>{
        this.lgraficas=result;
        for(let item of this.lgraficas.rows){

            this.listaTotal.push(item.total)
            this.listaCandidatos.push(item.nombre)
            let barras={
              data:[0,item.total],
              label:[item.nombre]
            }
            this.listabarras.push(barras);
        }

       this.pieChartLabels=this.listaCandidatos;
      this.pieChartData = this.listaTotal;
      this.barChartData=this.listabarras;
    },error=>{
      console.log(error)
    }
  );
}


Marcador(nombre:any){
  //console.log(nombre.label)
  this.servResult.ObtenerUbicacion(nombre.label).subscribe(
    result=>{
        this.listaubicacion=result;
        if(this.listaubicacion.rows.length==0){
          this.errorUbicacion="Error La Ubicacion que seleccionó, no tiene Eleccion"
        }else{
          this.rout.navigate(['/ganador/'+this.listaubicacion.rows[0].id_eleccion])
        }
        
    },error=>{
        console.log(error)
        this.errorUbicacion="Error La Ubicacion que seleccionó, no tiene Eleccion"
    }
  );
}



}
