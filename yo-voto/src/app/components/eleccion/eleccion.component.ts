import { Component, OnInit } from '@angular/core';
import { EleccionService } from '../../services/eleccion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eleccion',
  templateUrl: './eleccion.component.html',
  styleUrls: ['./eleccion.component.css']
})
export class EleccionComponent implements OnInit {

  constructor(private EleccionService: EleccionService,private router: Router) { }
  public cui = ""
  public votacion = ""

  ngOnInit(): void {
    this.LasElecciones();
  }

  async Asignacion(){
    let res = await this.EleccionService.AsignarEleccion(this.cui, this.votacion)
    let json=JSON.stringify(res)
    let obj= JSON.parse(json)
    alert(obj.mensaje)
  }

  Elecciones: any;
  Eleccion: any;
  async LasElecciones(){
    let res = await this.EleccionService.getElecciones()
    let json=JSON.stringify(res)
    let obj= JSON.parse(json)
    this.Elecciones = obj;
  }

}
