import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  user = {
    cui:null,
    correo: null
  };

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  signIn(){
    console.log(this.user)
    if(this.user.cui == 'grupo11' && this.user.correo == 'gR4p0_11'){
      localStorage.setItem('token', 'administrador')
      window.location.reload()
    }else {
      this.authService.signIn(this.user)
      .subscribe(
        res => {
          console.log(res);
          localStorage.setItem('token', res.token);
          window.location.reload()
          this.router.navigate(['Eleccion']);
        },
        err => console.log(err)
      )
    }
    /**/
  
  }

}
