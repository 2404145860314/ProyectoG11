import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEleccionesComponent } from './lista-elecciones.component';

describe('ListaEleccionesComponent', () => {
  let component: ListaEleccionesComponent;
  let fixture: ComponentFixture<ListaEleccionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaEleccionesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListaEleccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
