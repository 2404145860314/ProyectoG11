import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ResultadosService } from 'src/app/services/resultados.service';

@Component({
  selector: 'app-lista-elecciones',
  templateUrl: './lista-elecciones.component.html',
  styleUrls: ['./lista-elecciones.component.css']
})
export class ListaEleccionesComponent implements OnInit {

  constructor(private elecServ:ResultadosService) { }
  listaElecciones:any=[];
  hora_ini:any;
  hora_fin:any;
  fecha_inicio:any;
  fecha_fin:any;

  ngOnInit(): void {
    this.ObtenerElecciones()

  }

 ObtenerElecciones(){
  this.elecServ.ObtenerElecciones().subscribe(

    result=>{
          this.listaElecciones=result;
          console.log("dfds")


    },error=>{
        console.log(error)
    }
  )

 }

}
