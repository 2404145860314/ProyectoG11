import { Component, OnInit } from '@angular/core';
import { ResultadosService } from 'src/app/services/resultados.service';

@Component({
  selector: 'app-auditoria',
  templateUrl: './auditoria.component.html',
  styleUrls: ['./auditoria.component.css']
})
export class AuditoriaComponent implements OnInit {

  constructor(private servResult:ResultadosService) { }

  ngOnInit(): void {

        this.getAuditoria()
  }

  listaAudit:any=[]

  getAuditoria(){
    this.servResult.ObtenerAuditoria().subscribe(
        result=>{
            this.listaAudit=result;
            console.log(this.listaAudit.rows)
        },error=>{
            console.log(error)
        }
    );
  }



}
