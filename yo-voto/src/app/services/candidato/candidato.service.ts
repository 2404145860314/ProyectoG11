import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { path } from '../../config/config.module';

@Injectable({
  providedIn: 'root'
})
export class CandidatoService {
  private basepath:string = path.path;
  constructor(private http: HttpClient) { }

  private async request(method: string, url: string, data?: any) {
    
    const result = this.http.request(method, url, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: {
        Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.synVIXnXxXMBeS71gN5e6txkWz8x25iXPqcpJcK754w'
      }
    });
    return new Promise((resolve, reject) => {
      result.subscribe(resolve, reject);
    });
  }

  crearCandidato(eleccion:any) {
    return this.request('POST', `${this.basepath}/candidato/create`, eleccion);
  }

  getCandidatos(id_eleccion:any) {
    return this.request('GET', `${this.basepath}/candidato/get_all/${id_eleccion}`);
  }

  getDepartamentos() {
    return this.request('GET', `${this.basepath}/departamento/get_all`)
  }

  getMunicipios(id_departamento:any) {
    return this.request('GET', `${this.basepath}/municipio/get_all/${id_departamento}`)
  }
}
