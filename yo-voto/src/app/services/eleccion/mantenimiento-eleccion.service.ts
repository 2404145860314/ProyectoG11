import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { path } from '../../config/config.module';

@Injectable({
  providedIn: 'root'
})
export class MantenimientoEleccionService {

  private basepath:string = path.path;
  private basepath2:string = path.path2;
  constructor(private http: HttpClient) { }

  private async request(method: string, url: string, data?: any) {
    
    const result = this.http.request(method, url, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: {
        Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.synVIXnXxXMBeS71gN5e6txkWz8x25iXPqcpJcK754w'
      }
    });
    return new Promise((resolve, reject) => {
      result.subscribe(resolve, reject);
    });
  }

  crearEleccion(eleccion:any) {
    return this.request('POST', `${this.basepath}/eleccion/create`, eleccion);
  }

  getElecciones() {
    return this.request('GET', `${this.basepath}/eleccion/get_all`);
  }


  createRENAP(registro:any) {
    return this.request('POST',`${this.basepath2}/Registro`, registro)
  }
  
}
