import { TestBed } from '@angular/core/testing';

import { MantenimientoEleccionService } from './mantenimiento-eleccion.service';

describe('MantenimientoEleccionService', () => {
  let service: MantenimientoEleccionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MantenimientoEleccionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
