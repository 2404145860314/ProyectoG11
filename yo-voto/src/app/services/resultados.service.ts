import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResultadosService {

  constructor(private http:HttpClient) { }
  API_URI='http://34.132.211.205:3002';


  ObtenerElecciones(){
    return this.http.get(`${this.API_URI}/resultados/elecciones`);
  }

  ObtenerGanador(id:any){
    return this.http.get(`${this.API_URI}/ganador/`+id);
  }
  ObtenerRegistrados(){
    return this.http.get(`${this.API_URI}/api/registradas`);
  }
  ObtenerVotaciones(){
    return this.http.get(`${this.API_URI}/api/totalvotacion`);
  }

  ObtenerCandidatos(id:any){
    return this.http.get(`${this.API_URI}/api/graficapyb/`+id);
  }

  ObtenerUbicacion(ubicacion:any){
    return this.http.post(`${this.API_URI}/api/eleccion/ubicacion`,{ubicacion:ubicacion});
  }
  ObtenerAuditoria(){
    return this.http.get(`${this.API_URI}/api/auditoria`);
  }

}
