import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class EleccionService {

  url:string="http://34.132.211.205:2000/";//"bL-475949248.us-east-2.elb.amazonaws.com:3000/"
  constructor(private http: HttpClient) { }

  getElecciones(){
    const data = {}
    return this.http.post(this.url+'elecciones',data).toPromise()
  }

  AsignarEleccion(id_persona: string, id_eleccion: string){
    const data = {id_persona, id_eleccion}
    return this.http.post(this.url+'AsignarPersonaEleccion',data).toPromise()
  }

}