import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class VotoService{
    url:string="http://34.132.211.205:2000";//"bL-475949248.us-east-2.elb.amazonaws.com:3000/"
    constructor(private http: HttpClient) { }

    getVotaciones(id_persona:string){
        const data = {id_persona}
        return this.http.post(this.url+"/GetVotaciones", data).toPromise()
    }

    getPostulados(id_eleccion:string){
      const data = {id_eleccion}
      return this.http.post(this.url+"/GetPostulados", data).toPromise()
    }

    SendVoto(id_candidato:string, id_persona:string, id_eleccion:string){
      var estado:string, aprobado:number, ubicacion:string
      estado = ""
      aprobado = 0
      ubicacion = ""
      const data = {estado, aprobado, ubicacion, id_candidato, id_persona, id_eleccion}
      return this.http.post(this.url+"/Votar", data).toPromise()
    }
}