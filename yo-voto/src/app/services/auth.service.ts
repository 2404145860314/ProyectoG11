import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  
  private URL = 'http://34.132.211.205:3000';
  private RegistroUser = 'http://34.132.211.205:2000';
  private ConsultarExpediente = 'http://34.132.211.205:2500';

  constructor(private http: HttpClient) { }

  signUp(user: any) {
    return this.http.post<any>(this.RegistroUser + '/RegistrarPersona', user);
  }

  signIn(user: any) {
    return this.http.post<any>(this.URL + '/autenticacion/login', user);
  }

  validarUserRenap(user: any){
    return this.http.post<any>(this.ConsultarExpediente + '/ConsultarExpediente', user);
  }


}