import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';
import { SigninComponent } from './components/signin/signin.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { ListaEleccionesComponent } from './components/lista-elecciones/lista-elecciones.component';
import { GanadorComponent } from './components/ganador/ganador.component';
import { ChartsModule } from 'ng2-charts';
import { AgmCoreModule } from "@agm/core";
import { AuditoriaComponent } from './components/auditoria/auditoria.component';
import { EleccionComponent } from './components/eleccion/eleccion.component';
import { MantenimientoCandidatoComponent } from './components/mantenimiento-candidato/mantenimiento-candidato.component';
import { MantenimientoEleccionComponent } from './components/mantenimiento-eleccion/mantenimiento-eleccion.component';
import { VotosComponent } from './components/votos/votos.component';
import { RenapMasivoComponent } from './components/renap-masivo/renap-masivo.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    TasksComponent,
    ListaEleccionesComponent,
    GanadorComponent,
    AuditoriaComponent,
    EleccionComponent,
    MantenimientoCandidatoComponent,
    MantenimientoEleccionComponent,
    VotosComponent,
    RenapMasivoComponent,
    EleccionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ChartsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCJ4wO1-9Qz8nv4s0f92cfIGqStrIWUCqs',
      libraries: ['places']
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
