import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'yo-voto';
  logueado: boolean = false;
  admin: boolean = false;
  constructor() {
    if (localStorage.getItem('token') != null) {
      if (localStorage.getItem('token') == 'administrador') {
        this.admin = true;
      }
      this.logueado = true;
    }
  }

  salir() {
    localStorage.removeItem('token')
    window.location.reload()
  }
}
