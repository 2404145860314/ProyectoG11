var express = require('express')
const jwt = require('jsonwebtoken')

// middleware to validate token (rutas protegidas)
const verifyToken = (req, res, next) => {
    try {
		if (!req.headers.authorization) {
			return res.status(401).send('Request no Autorizado');
		}
		let token = req.headers.authorization.split(' ')[1];
		if (token === 'null') {
			return res.status(401).send('Request no Autorizado');
		}

		const payload = jwt.verify(token, 'savacacionessisale');
        //console.log(payload);
		req.email = payload.email;
		next();
		//if (!payload) {
		//	return res.status(401).send('Unauhtorized Request');
		//}

		//req.userId = payload._id;
		//next();
	} catch(e) {
		//console.log(e)
		return res.status(401).send('Unauhtorized Request');
	}
}

module.exports = verifyToken;