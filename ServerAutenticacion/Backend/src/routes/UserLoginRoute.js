var express = require('express')
var login = express.Router();
var controller = require('../controllers/UserLoginController');

login.post('/autenticacion/login', controller.default.getInstance().get_login);
module.exports = login;