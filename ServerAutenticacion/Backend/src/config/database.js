var mysql = require('mysql');
var conexion= mysql.createConnection({
    host : '34.121.159.67',
    port: '3306',
    database : 'saproyecto',
    user : 'root',
    password : 'root',
});

conexion.connect(function(err) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + conexion.threadId);
});

module.exports = conexion;