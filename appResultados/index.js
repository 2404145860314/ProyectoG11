const express = require('express');
const mysql = require('mysql');
const nodemailer = require("nodemailer");
const app = express();
const dateShortcode = require('date-shortcode')

const fileUpload = require('express-fileupload')
var cors = require('cors')
var morgan=require('morgan')
var bodyParser = require('body-parser')

var AWS = require('aws-sdk');

app.use(express.static('files'));
//app.use(bodyParser());
app.use(fileUpload())
app.use(cors())

app.use(bodyParser.json({limit: "90mb"}));
app.use(bodyParser.urlencoded({limit: "90mb", extended: true, parameterLimit:50000}));

app.use(morgan("dev"));

const connection = mysql.createPool({
	connectionLimit: 10,
	host: process.env.MYSQL_HOST || '34.121.159.67',
	user: process.env.MYSQL_USER || 'root',
	password: process.env.MYSQL_PASSWORD || 'root',
	database: process.env.MYSQL_DATABASE || 'saproyecto'
});

app.get('/resultados/elecciones', async(req, res) => {
	connection.query('SELECT * FROM Eleccion' , (err, rows) => {
		if(err){
			res.json({
				success: false,
				err
				});
		}
		else{
			console.log("success")
			let pila=[]
			for( let item in rows){
				eleccion={
					id:rows[item].id_eleccion,
					titulo:rows[item].titulo,
					fecha_ini:rows[item].fecha_ini,
					fecha_fin:rows[item].fecha_fin
				}
				pila.push(eleccion)		
			}
			res.json({response:pila}).status(200);
		}
	});
});

app.get('/api/eleccion/:id',async (req, res) => {
	let id_eleccion=req.params.id
	connection.query('SELECT * FROM Eleccion where id_eleccion='+id_eleccion, (err, rows) => {
		if(err){
			res.json({
				success: false,
				err
				});
		}
		else{
			console.log("success")
			res.json({success: true,rows});
		}
	});
});


app.get('/ganador/:id', async(req, res) => {
	let id_eleccion=req.params.id
	let querymysql='select count(*) as total,c.* from Eleccion as e join Candidato as c on e.id_eleccion=c.id_eleccion'
	querymysql+=' join Votacion as v on v.id_candidato=c.cui'
	querymysql+=' where e.id_eleccion='+id_eleccion
	querymysql+=' group by v.id_candidato'
	querymysql+=' order by 1 desc'
	querymysql+=' limit 1'

	connection.query(querymysql, (err, rows) => {
		if(err){
			res.json({success: false,err});
		}
		else{
			console.log("success")
			connection.query('select count(*) as total from Votacion',(err2,rows2)=>{
				if(err2){
					res.json({success: false,err2});
				}
				else{
					let totalganador=rows[0].total
					let totalcompleto=rows2[0].total
					res.json({success: true,total:totalganador,porcentaje:totalganador/totalcompleto,rows});
				}		
			})
			
		}
	});
	
});




app.get('/api/registradas',async (req, res) => {
	let id_eleccion=req.params.id
	connection.query('SELECT count(*) as total FROM Persona', (err, rows) => {
		if(err){
			res.json({
				success: false,
				err
				});
		}
		else{
			console.log("success")
			console.log(rows)
			res.json({success: true,rows});
		}
	});
});


app.get('/api/totalvotacion', async(req, res) => {
	let id_eleccion=req.params.id
	connection.query('SELECT count(*) as total FROM Votacion', (err, rows) => {
		if(err){
			res.json({
				success: false,
				err
				});
		}
		else{
			console.log("success")
			console.log(rows)
			res.json({success: true,rows});
		}
	});
});

app.get('/api/graficapyb/:id',async (req, res) => {
	let id_eleccion=req.params.id
	let querymysql='(select count(*) as total,c.* from Eleccion as e  join Candidato as c on e.id_eleccion=c.id_eleccion'
	querymysql+=' join Votacion as v on v.id_candidato=c.cui'
	querymysql+=' where e.id_eleccion='+id_eleccion
	querymysql+=' group by v.id_candidato'
	querymysql+=' order by 1 desc)union'
	querymysql+=' (select 0 as total, c.* from Eleccion as e join Candidato as c left join Votacion as v on v.id_candidato=c.cui'
	querymysql+=' where (v.id_candidato is NULL and c.id_eleccion='+id_eleccion
	querymysql+=' ))'
	connection.query(querymysql, (err, rows) => {
		if(err){
			res.json({
				success: false,
				err
				});
		}
		else{
			console.log("success")
			res.json({rows});
		}
	});
});

app.get('/api/auditoria', async(req, res) => {
	let id_eleccion=req.params.id
	let querymysql='(select count(*) as total,c.*,e.titulo from Eleccion as e  join Candidato as c on e.id_eleccion=c.id_eleccion'
	querymysql+=' join Votacion as v on v.id_candidato=c.cui'
	querymysql+=' group by v.id_candidato'
	querymysql+=' order by e.titulo desc)union'
	querymysql+=' (select 0 as total, c.*,ei.titulo from Eleccion as ei join Candidato as c left join Votacion as v on v.id_candidato=c.cui'
	querymysql+=' where (v.id_candidato is NULL'
	querymysql+=' )'
	querymysql+=' order by ei.titulo desc'
	querymysql+=' )'
	connection.query(querymysql, (err, rows) => {
		if(err){
			res.json({
				success: false,
				err
				});
		}
		else{
			console.log("success")
			res.json({success: true,rows}).status(200);
		}
	});
});


app.post('/api/eleccion/ubicacion', async(req, res) => {
	let ubicacion=req.body.ubicacion;
	let querymysql='select * from Eleccion where ubicacion=\''+ubicacion+'\''
	connection.query(querymysql, (err, rows) => {
		if(err){
			res.json({success: false,err});
		}
		else{
			res.json({success: true,rows});
		}		
	})
	
});

module.exports=app.listen(3002, () => console.log('listining on port 3002'));