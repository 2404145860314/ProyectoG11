var database = require("../config/db.config");

var MunicipioController = (function () {
    function MunicipioController() {
        /* consultar elecciones*/
        this.get_all = function (req, res) {

            var query = "SELECT * FROM Municipio where id_departamento = ?  "
            var id = req.params.id
            database.query(query, [id],function (err, data) {
                if (err) {
                    res.json([]);
                } else {
                    res.json(data);
                }
            });
        };

    }
    MunicipioController.getInstance = function () {
        return this._instance || (this._instance = new this());
    };
    return MunicipioController;
}());
exports.default = MunicipioController;