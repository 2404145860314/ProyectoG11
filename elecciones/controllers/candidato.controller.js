var database = require("../config/db.config");
const aws_keys = require('../config/cred.config');
var aws = require('aws-sdk');
const s3 = new aws.S3(aws_keys.s3);

var CandidatoController = (function () {
    function CandidatoController() {
        /* consultar candidatos*/
        this.get_one = function (req, res) {

            var query = "SELECT cui, nombre, Partido, foto,imagen_partido, departamento, municipio FROM Candidato WHERE id_eleccion = ?"
            var id = req.params.id
            database.query(query, [id], function (err, data) {
                if (err) {
                    res.json({
                        estado: false,
                        status: 400,
                        error: err
                    });
                } else {
                    res.json({
                        estado: true,
                        data: data,
                        status: 200
                    });
                }
            });
        };

        this.create = async function (req, res) {
            var id = req.body.cui + 'foto';
            var foto = req.body.foto;     //base64
            //console.log(req.body)
            //carpeta y nombre que quieran darle a la imagen
            var nombrei = "fotos-usuario/" + id + ".jpg";

            //se convierte la base64 a bytes
            let buff = new Buffer.from(foto, 'base64');



            const params = {
                Bucket: "sa-img",
                Key: nombrei,
                Body: buff,
                ContentType: "image",
                ACL: 'public-read'
            };

            var fotobd = ''
            await s3.upload(params, async function (err, data) {
                if (err) {
                    console.log('Error uploading file:', err);
                    res.json({
                        estado: false,
                        status: 400,
                        error: err
                    });
                } else {

                    fotobd = data.Location
                    console.log(fotobd + 'fotodentro')
                    var id2 = req.body.cui + 'partido';
                    var foto2 = req.body.imagen_partido;     //base64

                    //carpeta y nombre que quieran darle a la imagen
                    var nombrei2 = "fotos-partido/" + id2 + ".jpg";

                    //se convierte la base64 a bytes
                    let buff2 = new Buffer.from(foto2, 'base64');



                    const params2 = {
                        Bucket: "sa-img",
                        Key: nombrei2,
                        Body: buff2,
                        ContentType: "image",
                        ACL: 'public-read'
                    };


                    var fotobd2 = ''
                    await s3.upload(params2, function (err, data) {
                        if (err) {
                            console.log('Error uploading file:', err);
                            res.json({
                                estado: false,
                                status: 400,
                                error: err
                            });
                        } else {
                            console.log('Url del objetot:', data.Location);
                            fotobd2 = data.Location



                            var query = "insert into Candidato(cui, nombre, Partido, foto, imagen_partido, departamento, municipio, id_eleccion) values (?,?,?,?,?,?,?,?);"
                            var body = {
                                cui: req.body.cui,
                                nombre: req.body.nombre,
                                partido: req.body.partido,
                                foto: fotobd,
                                imagen_partido: fotobd2,
                                departamento: req.body.departamento,
                                municipio: req.body.municipio,
                                id_eleccion: req.body.id_eleccion
                            }

                            database.query(query, [body.cui, body.nombre,body.partido, body.foto, body.imagen_partido, body.departamento, body.municipio, body.id_eleccion], function (err, data) {
                                if (err) {
                                    res.status(400).json({
                                        estado: false,
                                        status: 400,
                                        error: err
                                    });
                                } else {
                                    res.json({
                                        estado: true,
                                        status: 200,
                                        mensaje: "El usuario se agrego con exito"
                                    });
                                }
                            });
                        }
                    });
                }
            });


        }
    }
    CandidatoController.getInstance = function () {
        return this._instance || (this._instance = new this());
    };
    return CandidatoController;
}());
exports.default = CandidatoController;