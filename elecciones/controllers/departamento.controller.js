var database = require("../config/db.config");

var DepartamentoController = (function () {
    function DepartamentoController() {
        /* consultar elecciones*/
        this.get_all = function (req, res) {

            var query = "SELECT * FROM Departamento"
            database.query(query, function (err, data) {
                if (err) {
                    res.json([]);
                } else {
                    res.json(data);
                }
            });
        };

    }
    DepartamentoController.getInstance = function () {
        return this._instance || (this._instance = new this());
    };
    return DepartamentoController;
}());
exports.default = DepartamentoController;