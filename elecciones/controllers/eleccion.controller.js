var database = require("../config/db.config");

var EleccionController = (function () {
    function EleccionController() {
        /* consultar elecciones*/
        this.get_all = function (req, res) {

            var query = "SELECT id_eleccion, titulo, descripcion, fecha_ini, fecha_fin, ubicacion FROM Eleccion "

            database.query(query, function (err, data) {
                if (err) {
                    res.json([]);
                } else {
                    res.json(data);
                }
            });
        };

        /* Crear Elecciones */
        this.create = async function (req, res) {
            var query = "insert into Eleccion(titulo, descripcion, fecha_ini, fecha_fin, ubicacion) values (?,?,?,?,?);"
            var body = {
                titulo: req.body.titulo,
                descripcion: req.body.descripcion,
                fecha_ini: req.body.fecha_ini,
                fecha_fin: req.body.fecha_fin,
                ubicacion: req.body.ubicacion
            }

            database.query(query, [body.titulo, body.descripcion, body.fecha_ini, body.fecha_fin, body.ubicacion], function (err, data) {
                if (err) {
                    res.status(400).json({
                        estado: false,
                        status: 400,
                        error: err
                    });
                } else {
                    res.json({
                        estado: true,
                        status: 200,
                        mensaje: "La elección se agregó con éxito"
                    });
                }
            });
        }
    }
    EleccionController.getInstance = function () {
        return this._instance || (this._instance = new this());
    };
    return EleccionController;
}());
exports.default = EleccionController;