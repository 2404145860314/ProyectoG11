const express = require('express')
var bodyParser = require('body-parser');
var cors = require('cors');
const jwt = require('jsonwebtoken')
const app = express()


//Body-Parser Config
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ limit: '50mb' }));
app.use(cors())

//Headers
app.use(function(req, res, next) {
    
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    if (req.methods == "OPTIONS") {
        res.sendStatus(200);
    } else {
        next();
    }
});


const verifyToken = (req, res, next) => {
    const token = req.header('Authorization')
    if (!token) return res.status(401).json({ error: 'Acceso denegado' })
    try {
        const verified = jwt.verify(token, 'savacacionessisale')
        req.user = verified
        next() // continuamos
    } catch (error) {
        res.status(400).json({error: 'token no es válido'})
    }
} 

var eleccion = require('./routes/eleccion.route');
var candidato = require('./routes/candidato.route');
var municipio = require('./routes/municipio.route');
var departamento = require('./routes/departamento.route');
app.use("/", verifyToken, candidato);
app.use("/", verifyToken,eleccion);
app.use("/", verifyToken,municipio);
app.use("/", verifyToken,departamento);



app.set('port', process.env.PORT || 5000)
module.exports = app;