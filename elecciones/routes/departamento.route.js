var express = require('express')
var departamento = express.Router();
var controller = require('../controllers/departamento.controller');

departamento.get('/departamento/get_all', controller.default.getInstance().get_all);
module.exports = departamento;