var express = require('express')
var municipio = express.Router();
var controller = require('../controllers/municipio.controller');

municipio.get('/municipio/get_all/:id', controller.default.getInstance().get_all);
module.exports = municipio;